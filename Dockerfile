# GNU Radio Docker image
#
# Copyright (C) 2020-2024 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG DEBIAN_IMAGE_TAG=bookworm-slim
FROM debian:${DEBIAN_IMAGE_TAG} AS slim
LABEL org.opencontainers.image.authors='Vasilis Tsiligiannis <acinonyx@openwrt.gr>'

# Copy packages list
COPY packages-slim.debian /usr/local/src/docker-gnuradio/

# Install system packages
RUN apt-get update \
	&& xargs -a /usr/local/src/docker-gnuradio/packages-slim.debian apt-get install -qy \
	&& rm -r /var/lib/apt/lists/*

FROM slim AS satnogs

# Add SatNOGS repository
RUN apt-get update \
	&& apt-get -qy install curl \
	&& echo "deb [signed-by=/etc/apt/keyrings/satnogs.asc] https://download.opensuse.org/repositories/home:/librespace:/satnogs2/Debian_12/ /" > /etc/apt/sources.list.d/satnogs.list \
	&& mkdir -p /etc/apt/keyrings/ \
	&& curl -fsL -o /etc/apt/keyrings/satnogs.asc "https://download.opensuse.org/repositories/home:librespace:satnogs2/Debian_12/Release.key" \
	&& rm -r /var/lib/apt/lists/*

# Copy packages list
COPY packages-satnogs.debian /usr/local/src/docker-gnuradio/

# Install system packages
RUN apt-get update \
	&& xargs -a /usr/local/src/docker-gnuradio/packages-satnogs.debian apt-get install -qy \
	&& rm -r /var/lib/apt/lists/*

# Download USRP images
RUN uhd_images_downloader -q

FROM satnogs AS satnogs-unstable

# Add SatNOGS repository
RUN apt-get update \
	&& apt-get -qy install curl \
	&& echo "deb [signed-by=/etc/apt/keyrings/satnogs.asc] https://download.opensuse.org/repositories/home:/librespace:/satnogs-unstable2/Debian_12/ /" > /etc/apt/sources.list.d/satnogs.list \
	&& mkdir -p /etc/apt/keyrings/ \
	&& curl -fsL -o /etc/apt/keyrings/satnogs.asc "https://download.opensuse.org/repositories/home:librespace:satnogs-unstable2/Debian_12/Release.key" \
	&& rm -r /var/lib/apt/lists/*

# Copy packages list
COPY packages-satnogs-unstable.debian /usr/local/src/docker-gnuradio/

# Install system packages
RUN apt-get update \
	&& xargs -a /usr/local/src/docker-gnuradio/packages-satnogs-unstable.debian apt-get install -qy \
	&& rm -r /var/lib/apt/lists/*

# Download USRP images
RUN uhd_images_downloader -q

FROM satnogs

# Copy packages list
COPY packages.debian /usr/local/src/docker-gnuradio/

# Install system packages
RUN apt-get update \
	&& xargs -a /usr/local/src/docker-gnuradio/packages.debian apt-get install -qy \
	&& rm -r /var/lib/apt/lists/*
